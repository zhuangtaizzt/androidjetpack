package com.mvtrail.sunflower.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mvtrail.sunflower.data.GardenPlantingRepository
import com.mvtrail.sunflower.data.PlantAndGardenPlantings

class GardenPlantingListViewModel internal constructor(
    gardenPlantingRepository: GardenPlantingRepository
) : ViewModel() {

    val gardenPlantings = gardenPlantingRepository.getGardenPlantings()

    val plantAndGardenPlantings: LiveData<List<PlantAndGardenPlantings>> =
        Transformations.map(gardenPlantingRepository.getPlantAndGardenPlantings()) { plantings ->
            plantings.filter { it.gardenPlantings.isNotEmpty() }
        }
}