package com.mvtrail.sunflower2.db;

import java.io.Serializable;
import java.util.Calendar;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(
        tableName = "garden_plantings",
        foreignKeys = {@ForeignKey(entity = Plant.class, parentColumns = {"id"}, childColumns = {"plant_id"})},
        indices = {@Index("plant_id")}
)
public class GardenPlanting implements Serializable {
    @ColumnInfo(name = "plant_id")
    private String plantId;

    @ColumnInfo(name = "plant_date")
    private Calendar plantDate = Calendar.getInstance();

    @ColumnInfo(name = "last_watering_date")
    Calendar lastWatringDate = Calendar.getInstance();

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Long gardenPlantingId = 0L;

    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public Calendar getPlantDate() {
        return plantDate;
    }

    public void setPlantDate(Calendar plantDate) {
        this.plantDate = plantDate;
    }

    public Calendar getLastWatringDate() {
        return lastWatringDate;
    }

    public void setLastWatringDate(Calendar lastWatringDate) {
        this.lastWatringDate = lastWatringDate;
    }

    public Long getGardenPlantingId() {
        return gardenPlantingId;
    }

    public void setGardenPlantingId(Long gardenPlantingId) {
        this.gardenPlantingId = gardenPlantingId;
    }
}
