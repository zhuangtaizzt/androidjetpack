package com.mvtrail.sunflower2.viewmodel;

import com.mvtrail.sunflower2.db.GardenPlantingRepository;
import com.mvtrail.sunflower2.db.PlantAndGardenPlantings;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

public class GardenViewModel extends ViewModel {
    private GardenPlantingRepository gardenPlantingRepository;
    private LiveData<List<PlantAndGardenPlantings>> gardenPlantings;
    private LiveData<List<PlantAndGardenPlantings>> plantAndGardenPlantings;

    public GardenViewModel(GardenPlantingRepository gardenPlantingRepository) {
        this.gardenPlantingRepository = gardenPlantingRepository;
    }

    public LiveData<List<PlantAndGardenPlantings>> getPlantAndGardenPlantings() {

        Transformations
                .map(gardenPlantingRepository.getPlantAndGardenPlantings(), new Function<List<PlantAndGardenPlantings>, List<PlantAndGardenPlantings>>() {
                    @Override
                    public List<PlantAndGardenPlantings> apply(List<PlantAndGardenPlantings> input) {
                        List<PlantAndGardenPlantings> result = new ArrayList<>();
                        for (PlantAndGardenPlantings item : input) {
                            if (!item.getGardenPlantings().isEmpty()) {
                                result.add(item);
                            }
                        }
                        return result;
                    }
                });
        return plantAndGardenPlantings;
    }


}
