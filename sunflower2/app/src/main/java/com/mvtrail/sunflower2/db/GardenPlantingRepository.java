package com.mvtrail.sunflower2.db;

import com.mvtrail.sunflower2.utils.AppExecutors;

import java.util.List;

import androidx.lifecycle.LiveData;

public class GardenPlantingRepository {

    private static volatile GardenPlantingRepository sInstance;

    public synchronized GardenPlantingRepository instance() {
        if (sInstance == null) {
            sInstance = new GardenPlantingRepository();
        }
        return sInstance;
    }

    private GardenPlantingDao gardenPlantingDao;

    public void createGardenPlanting(String plantId) {
        AppExecutors.runOnIoThread(new Runnable() {
            @Override
            public void run() {
                GardenPlanting gardenPlanting = new GardenPlanting();
                gardenPlanting.setPlantId(plantId);
                gardenPlantingDao.insertGardenPlanting(gardenPlanting);
            }
        });
    }

    public void removeGardenPlanting(GardenPlanting gardenPlanting) {
        AppExecutors.runOnIoThread(new Runnable() {
            @Override
            public void run() {
                gardenPlantingDao.deleteGardenPlanting(gardenPlanting);
            }
        });
    }

    public LiveData<GardenPlanting> getGardenPlantingForPlant(String plantId) {
        return gardenPlantingDao.getGardenPlantingForPlant(plantId);
    }

    public LiveData<List<GardenPlanting>> getGardenPlantings() {
        return gardenPlantingDao.getGardenPlantings();
    }

    public LiveData<List<PlantAndGardenPlantings>> getPlantAndGardenPlantings() {
        return gardenPlantingDao.getPlantAndGardenPlantings();
    }

}
