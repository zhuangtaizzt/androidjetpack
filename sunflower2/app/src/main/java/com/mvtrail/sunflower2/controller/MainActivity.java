package com.mvtrail.sunflower2.controller;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import com.mvtrail.sunflower2.R;
import com.mvtrail.sunflower2.databinding.ActivityMainBinding;
import com.mvtrail.sunflower2.utils.MyLocation;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_CODE = 0x100;

    private MyLocation mMyLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));

        mMyLocation = new MyLocation(this.getApplicationContext(), getLifecycle(), new MyLocation.LocationCallback() {
            @Override
            public void onLocationChanged(Location location) {
                String title = String.format("Latitude:%f, Longtitude:%f",
                        location.getLatitude(), location.getLongitude());
                getSupportActionBar().setTitle(title);
            }
        });
        requestLocationPermissions(mMyLocation.getRequiredPermissions());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            mMyLocation.setEnabled(true);
            mMyLocation.start();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void requestLocationPermissions(String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> requestPermissions = new ArrayList<>();
            for (int i = 0; i < permissions.length; ++i) {
                if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions.add(permissions[i]);
                }
            }
            if (requestPermissions.size() > 0) {
                String[] missing = new String[requestPermissions.size()];
                requestPermissions.toArray(missing);
                requestPermissions(permissions, REQUEST_PERMISSION_CODE);
            } else {
                mMyLocation.setEnabled(true);
            }
        }
    }
}
