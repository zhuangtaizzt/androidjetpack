package com.mvtrail.sunflower2.worker;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.mvtrail.sunflower2.db.AppDatabase;
import com.mvtrail.sunflower2.db.Plant;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class SeedDatabaseWorker extends Worker {
    private static final String PLANT_DATA_FILENAME = "plants.json";

    public SeedDatabaseWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Type plantType = new TypeToken<List<Plant>>() {
        }.getType();
        JsonReader jsonReader = null;
        InputStream inputStream = null;
        try {
            inputStream = getApplicationContext()
                    .getAssets().open(PLANT_DATA_FILENAME);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            jsonReader = new JsonReader(inputStreamReader);
            List<Plant> plantList = new Gson().fromJson(jsonReader, plantType);
            AppDatabase database = AppDatabase.instance(getApplicationContext());
            database.getPlantDao().insertAll(plantList);
            return Result.SUCCESS;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.FAILURE;
    }
}
