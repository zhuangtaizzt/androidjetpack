package com.mvtrail.sunflower2.db;

import java.util.Calendar;

import androidx.room.TypeConverter;

public class Converters {
    @TypeConverter
    public Long calendarToDatestamp(Calendar calendar) {
        return calendar.getTimeInMillis();
    }

    @TypeConverter
    public Calendar datestampToCalendar(Long value) {
        Calendar calender = Calendar.getInstance();
        calender.setTimeInMillis(value);
        return calender;
    }
}
