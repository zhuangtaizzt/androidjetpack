package com.mvtrail.sunflower2.utils;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class MyLocation implements LifecycleObserver {

    private static final String TAG = "MyLocation";
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private Context mContext;
    private MyLocationListener mLocationListener;
    private LocationManager mLocationManager;
    private String mProvider;
    private LocationCallback mCallback;
    private Lifecycle mLifecycle;
    private boolean mEnabled;

    public MyLocation(Context context, Lifecycle lifecycle, LocationCallback callback) {
        mLifecycle = lifecycle;
        mContext = context;
        mCallback = callback;
        lifecycle.addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void connect() {
        start();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void disconnect() {
        stop();
    }

    public void start() {
        if (!mEnabled) {
            return;
        }
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        //获取所有可用的位置提供器
        List<String> providerList = locationManager.getProviders(true);
        if (providerList.contains(LocationManager.GPS_PROVIDER)) {
            mProvider = LocationManager.GPS_PROVIDER;
        } else if (providerList.contains(LocationManager.NETWORK_PROVIDER)) {
            mProvider = LocationManager.NETWORK_PROVIDER;
        } else {
            //当没有可用的位置提供器时，弹出Toast提示用户
            Toast.makeText(mContext, "No Location provider to use", Toast.LENGTH_SHORT).show();
            return;
        }
        mLocationManager = locationManager;
        try {
            Location location = locationManager.getLastKnownLocation(mProvider);
            if (location != null && mCallback != null) {
                if (mLifecycle.getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    //显示当前设备的位置信息
                    mCallback.onLocationChanged(location);
                }
            }
            MyLocationListener locationListener = new MyLocationListener();
            locationManager.requestLocationUpdates(mProvider, 1000, 1, locationListener);
            mLocationListener = locationListener;
        } catch (SecurityException e) {
            //当没有可用的位置提供器时，弹出Toast提示用户
            Toast.makeText(mContext, "No Location permission！", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void stop() {
        if (mLocationManager != null) {
            //关闭程序时将监听移除
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    public String[] getRequiredPermissions() {
        return REQUIRED_PERMISSIONS;
    }

    //LocationListener 用于当位置信息变化时由 locationManager 调用
    class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            //更新当前设备的位置信息
            if (mCallback != null) {
                mCallback.onLocationChanged(location);
                Log.d("TAG", String.format("Get Location:%f, %f", location.getLongitude(), location.getLatitude()));
            }
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    public interface LocationCallback {

        void onLocationChanged(Location location);

    }
}
