package com.mvtrail.sunflower2.db;

import java.util.List;

import androidx.lifecycle.LiveData;

public class PlantRepository {
    private static volatile PlantRepository sInstance;

    private PlantDao plantDao;

    public synchronized PlantRepository instance() {
        if (sInstance == null) {
            sInstance = new PlantRepository();
        }
        return sInstance;
    }

    public LiveData<List<Plant>> getPlants() {
        return plantDao.getPlants();
    }

    public LiveData<Plant> getPlant(String plantId) {
        return plantDao.getPlant(plantId);
    }

    public LiveData<List<Plant>> getPlantsWithGrowZoneNumber(int growZoneNumber) {
        return plantDao.getPlantsWithGrowZoneNumber(growZoneNumber);
    }
}
